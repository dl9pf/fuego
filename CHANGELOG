This is the CHANGELOG file for the Fuego test framework

This file is in tbwiki markdown format, based on the Changelog format
described at: http://keepachangelog.com/

This file is for human-readable, high-level changes (not just a 
git log dump), with the intent to help people decide when they
want to upgrade Fuego versions.

Note that Fuego is distributed as two repositories, one with docker
and Jenkins stuff, and one with the fuego tests and script system.
This file serves as the common changelog for both repositories.

== [1.1.0] - 2017-04-07 ==
 * Move log directories
 * Add tests from AGL
   * These are experimental in this release
 * Move spec files into test directories
 * Move metrics files into test directories
 * Refactor interface with Jenkins
   * Create nodes and jobs with ftc tool
   * Put post_test into script system
   * Remove most plugins (groovy, dashboard-view, compact-cols and many more)
   * Handle proxies in container build
   * Use user UID as Jenkins uid inside container
 * Move fuego-core directory from /home/jenkins to /fuego-core
 * Move volume mounts and put more stuff outside the container
 * Add some tests of fuego itself (*.fuego_* tests)
 * Add new transports (ttc and serial)
   * These are experimental in this release
 * Lots of fixups to individual tests
 * Create arm toolchain download script
   * Don't put ARM toolchain into container automatically
 * Add more tools to container distribution of Debian
   * for various purposes
 * Add test server system (prototype)

== [1.0.9] - 2017-03-20 ==
 * Refactor Jenkins integration
   * create Jenkins nodes and jobs with a script
   * use fuego-ro and fuego-rw instead of userdata
   * put fuego-core outside container
 * add many ftc commands (for operating with server)
 * put test specs into test directories
 * support test packaging system (test.yaml file)
 * allow for a board-specified tmp directory

== [1.0.0] - 2016-11-08 ==
 * Add support for building the docker container behind a proxy
 * Add support for creating a container that can see USB changes
 * Change reboot test to only report boot time (not other stats)
 * Fix some other issues with the reboot test
 * Change name of nologger function and distribution file to 'nologread'
   * WARNING: this will break the Jenkins configuration for targets
   that referenced nologger.dist
     * the DISTRIB environment variable should be changed from
     'distribs/nologger.dist' to 'distribs/nologread.dist' in the target
     configuration for these boards
 * Add .gitignore files to make repository management easier
 * Add code to create the BOARD_TESTDIR directory if not already present
 * Change bzip2 test to support busybox version of bzip2
 * Add VERSION and CHANGELOG files
 * status:
   * This version of Fuego uses Jenkins 1.509.2
   * This verison of Fuego includes ftc version 0.3.0
